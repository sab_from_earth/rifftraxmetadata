# Rifftrax Metadata agent
This plex agent retrieves metadata for posters and movie descriptions from rifftrax.com, whilst getting all the other data from existing agents such as TheMovieDB. 

# Installation
Copy `RifftraxMetadata.bundle` to your plex plugin directory.

Go to Plex Server Settings > Agents > Rifftrax. Make sure that the Rifftax agent is at the top of the list.

Done. Enjoy.
