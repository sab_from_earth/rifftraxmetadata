# The Rifftrax agent
from rifftrax import *  # Putting all the logic into own file, otherwise Plex logging gets messy.


def Start():
    plugin_startup()


class RifftraxAgent(Agent.Movies):
    name = 'RiffTrax'
    identifier = 'RifftraxMetadata'
    languages = [Locale.Language.English, ]
    primary_provider = True
    accepts_from = [
        'com.plexapp.agents.themoviedb',
        'com.plexapp.agents.imdb',
    ]

    def search(self, results, media, lang, manual):
        rifftrax_search(results, media, lang, manual)

    def update(self, metadata, media, lang, force):
        rifftrax_update(metadata, media, lang, force)
