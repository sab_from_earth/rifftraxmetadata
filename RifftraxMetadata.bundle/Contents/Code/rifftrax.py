import difflib
import pprint
import re
import urllib
import unicodedata
from BeautifulSoup import BeautifulSoup


def plugin_startup():
    Log.Info("Rifftrax Agent indahouse.")


def get_rifftrax_data(movie_title):
    """Retrieve metadata from rifftrax.com for a given movie title."""
    # Make a request to the RiffTrax catalog search page for the movie title
    Log.Debug('Searching title: ' + movie_title)
    try:
        movie_title = unicodedata.normalize('NFKD', movie_title).encode('ASCII', 'ignore')
        Log.Warn('Stripping non-ascii characters: ' + movie_title)
    except TypeError:
        pass

    product_url = find_rifftrax_product_page(movie_title)
    if product_url is None:
        return None

    # Make a request to the product page and extract the description and poster URL
    Log.Debug('Getting riff details from ' + product_url)
    product_response = HTTP.Request(product_url).content
    product_soup = BeautifulSoup(product_response)
    poster_url = product_soup.find('img', {'src': lambda s: 'poster' in s})['src']

    description_div = product_soup.find('div', {'class': 'field-description'})
    description = ''
    for child in description_div.findAll(recursive=False):
        # Workaround for losing spaces around nested elements like <strong>
        text_nodes = [node for node in child.findAll(text=True)]
        description += ' '.join(text_nodes).strip() + '\n'

    # Return the description and poster URL as a dictionary
    return {'description': description.strip(), 'poster': poster_url}


def find_rifftrax_product_page(movie_title):
    search_url = 'https://www.rifftrax.com/catalog?' + urllib.urlencode({'search': movie_title})
    Log.Debug('Searching ' + search_url)
    search_response = HTTP.Request(search_url).content
    # Log.Debug(search_response)
    search_soup = BeautifulSoup(search_response)

    # Find the product page URL for the first search result (if any)
    pages = search_soup.findAll('div', {'class': 'product-grid'})
    if len(pages) is 0:
        Log.Error('No Riffs found for ' + movie_title)
        return None

    results = []
    for fragment in pages:
        url = fragment.find("a")["href"]
        # Remove stuff between parentheses
        title = re.sub(r'\([^\)]*\)', '', fragment.text).strip()
        similarity = difflib.SequenceMatcher(None, title.lower(), movie_title.lower()).ratio()
        results.append((url, title, similarity))
    results = sorted(results, key=lambda x: x[2], reverse=True)

    Log.Debug(results)
    return 'https://www.rifftrax.com' + results[0][0]


def rifftrax_search(results, media, lang, manual):
    search_dict = dict(id=media.id, name=media.name, year=media.year, lang=lang, manual=manual, get_imdb_id=True)
    Log.Debug([results, media, media.primary_metadata])
    Log.Debug('Rifftrax is forwarding search request to TMDB:')
    Log.Debug(search_dict)

    # Heavy lifting by TMDB:
    new_results = Core.messaging.call_external_function(
        'com.plexapp.agents.themoviedb',
        'MessageKit:GetTMDbSearchResults',
        kwargs=search_dict
    )

    if new_results:
        for result in new_results:
            #  use **result as a param instead?
            Log.Debug(result)
            results.Append(
                MetadataSearchResult(id=result['id'], name=result['name'], year=result['year'], lang=result['lang'],
                                     score=result['score']))
    else:
        Log.Info('TMDB search yielded no results.')


def rifftrax_update(metadata, media, lang, force):
    Log.Debug('Rifftrax Update called: (metadata, media, lang, force)')
    Log.Debug([metadata, media, lang, force])
    Log.Debug('=========== METADATA ===========')
    Log.Debug(pprint.pformat(metadata, depth=5))
    Log.Debug(dir(metadata))
    Log.Debug('=========== MEDIA ===========')
    Log.Debug(pprint.pformat(media, depth=5))
    Log.Debug(dir(media))

    riff_data = get_rifftrax_data(media.title)
    if riff_data is None:
        return
    Log.Debug(riff_data)

    Log.Debug('=========== METADATA DETAIL ===========')
    for attr_name, attr_obj in metadata.attrs.iteritems():
        if attr_name not in ['description', 'summary', 'title', 'posters']:
            Log.Debug('Skipping attribute ' + attr_name)
            continue
        Log.Debug({'name': attr_name, 'attr_obj': attr_obj})
        if attr_name is 'summary':
            Log.Debug('Summary before: ' + str(attr_obj))
            attr_obj.setcontent(riff_data['description'])
        elif attr_name is 'posters':
            Log.Debug('Posters!')
            for poster in attr_obj:
                Log.Debug(poster)
            attr_obj[riff_data['poster']] = Proxy.Preview(HTTP.Request(riff_data['poster']).content, sort_order=1000)
            metadata.posters.validate_keys([riff_data['poster']])

    #  Wishlist:
    #  Add Rifftrax tag
    #  Add Riffers as cast (requires external lookup + merge)
